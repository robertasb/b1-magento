<?php

class B1_Accounting_Model_Boolean
{

    public function toOptionArray()
    {
        return array(
            array('value' => 0, 'label' => Mage::helper('accounting')->__('No')),
            array('value' => 1, 'label' => Mage::helper('accounting')->__('Yes')),

        );
    }

}