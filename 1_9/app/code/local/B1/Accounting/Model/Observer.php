<?php

require_once(Mage::getBaseDir('lib') . '/B1/B1.php');

class B1_Accounting_Model_Observer
{

    public function syncAllProducts($observer)
    {
        try {
            Mage::log('syncAllProducts :: RUNNING', Zend_Log::INFO, 'b1.log');
            $this->syncB1ProductsWithShop(true);
            Mage::log('syncAllProducts :: FINISHED', Zend_Log::INFO, 'b1.log');
        } catch (Exception $ex) {
            Mage::logException($ex);
        }
        return $this;
    }

    public function syncUpdatedProducts($observer)
    {
        try {
            Mage::log('syncUpdatedProducts :: RUNNING', Zend_Log::INFO, 'b1.log');
            $this->syncB1ProductsWithShop(false);
            Mage::log('syncUpdatedProducts :: FINISHED', Zend_Log::INFO, 'b1.log');
        } catch (Exception $ex) {
            Mage::logException($ex);
        }
        return $this;
    }

    public function syncAllProductQuantities($observer)
    {
        try {
            Mage::log('syncAllProductQuantities :: RUNNING', Zend_Log::INFO, 'b1.log');
            $this->syncB1ProductQuantitiesWithShop(true);
            Mage::log('syncAllProductQuantities :: FINISHED', Zend_Log::INFO, 'b1.log');
        } catch (Exception $ex) {
            Mage::logException($ex);
        }
        return $this;
    }

    public function syncUpdatedProductQuantities($observer)
    {
        try {
            Mage::log('syncUpdatedProductQuantities :: RUNNING', Zend_Log::INFO, 'b1.log');
            $this->syncB1ProductQuantitiesWithShop(false);
            Mage::log('syncUpdatedProductQuantities :: FINISHED', Zend_Log::INFO, 'b1.log');
        } catch (Exception $ex) {
            Mage::logException($ex);
        }
        return $this;
    }

    public function syncOrders($observer)
    {
        try {
            Mage::log('syncOrders :: RUNNING', Zend_Log::INFO, 'b1.log');
            $this->syncShopOrdersWithB1();
            Mage::log('syncOrders :: FINISHED', Zend_Log::INFO, 'b1.log');
        } catch (Exception $ex) {
            Mage::logException($ex);
        }
        return $this;
    }

    public function resetStuckOrders($observer)
    {
        try {
            Mage::log('resetStuckOrders :: RUNNING', Zend_Log::INFO, 'b1.log');
            Mage::getModel('sales/order')->resetStuck();
            Mage::log('resetStuckOrders :: FINISHED', Zend_Log::INFO, 'b1.log');
        } catch (Exception $ex) {
            Mage::logException($ex);
        }
        return $this;
    }

    private function syncB1ProductsWithShop($syncAll = true)
    {
        $initialProductSyncDone = Mage::getStoreConfig('accounting/config/initial_product_sync_done');
        if (!$initialProductSyncDone && !$syncAll) {
            Mage::log("syncB1ProductsWithShop :: Initial product sync is not done yet.", Zend_Log::INFO, 'b1.log');
            return false;
        }
        $executionDateTime = date('Y-m-d H:i:s');
        $iteration = 0;
        $lastId = null;
        $apiKey = Mage::getStoreConfig('accounting/config/api_key');
        $privateKey = Mage::getStoreConfig('accounting/config/private_key');
        $maxRequestsToApi = Mage::getStoreConfig('accounting/config/max_requests_to_api_per_session');
        $listSize = Mage::getStoreConfig('accounting/config/list_size');
        $enableQuantitySync = Mage::getStoreConfig('accounting/config/enable_quantity_sync');
        $modAfterDate = $syncAll ? null : Mage::getStoreConfig('accounting/config/latest_product_sync_date');
        $b1 = new B1(['apiKey' => $apiKey, 'privateKey' => $privateKey]);
        if ($syncAll) {
            // Pre process.
            Mage::getModel('accounting/product')->markAllAsGhosts();
        }
        do {
            $iteration++;
            Mage::log("syncB1ProductsWithShop :: Iteration #$iteration", Zend_Log::INFO, 'b1.log');
            try {
                $response = $b1->exec('shop/product/list', array("lid" => $lastId, "lmod" => $modAfterDate));
            } catch (B1Exception $e) {
                print_r($e->getMessage());
                print_r($e->getExtraData());
            }
            if ($response === false) {
                // Log response error.
                throw new B1CronException('Error syncing products');
            }
            // Process response.
            $data = isset($response['data']) ? $response['data'] : array();
            foreach ($data as $i => $product) {
                $lastId = $product['id'];
                Mage::getModel('accounting/product')->import($product);
            }
        } while (!empty($data) && $response && count($data) == $listSize && $iteration < $maxRequestsToApi);
        if ($syncAll) {
            // Post process.
            Mage::getModel('accounting/product')->unlinkAllGhosts()->deleteGhosts();
            if (!$initialProductSyncDone) {
                Mage::getConfig()->saveConfig('accounting/config/initial_product_sync_done', 1)->cleanCache();
            }
        } else {
            // Remember execution datetime to use in the next run.
            Mage::getConfig()->saveConfig('accounting/config/latest_product_sync_date', $executionDateTime)->cleanCache();
        }
        if ($enableQuantitySync) {
            // Sync quantities if enabled in config.
            Mage::getModel('accounting/product')->syncQuantitiesWithShop();
        }
        return $this;
    }

    private function syncB1ProductQuantitiesWithShop($syncAll = true)
    {
        $initialProductSyncDone = Mage::getStoreConfig('accounting/config/initial_product_sync_done');
        if (!$initialProductSyncDone) {
            Mage::log("syncB1ProductQuantitiesWithShop :: Initial product sync is not done yet.", Zend_Log::INFO, 'b1.log');
            return false;
        }
        $executionDateTime = date('Y-m-d H:i:s');
        $iteration = 0;
        $lastId = null;
        $apiKey = Mage::getStoreConfig('accounting/config/api_key');
        $privateKey = Mage::getStoreConfig('accounting/config/private_key');
        $maxRequestsToApi = Mage::getStoreConfig('accounting/config/max_requests_to_api_per_session');
        $listSize = Mage::getStoreConfig('accounting/config/list_size');
        $enableQuantitySync = Mage::getStoreConfig('accounting/config/enable_quantity_sync');
        $modAfterDate = $syncAll ? null : Mage::getStoreConfig('accounting/config/latest_product_quantity_sync_date');

        if (!$enableQuantitySync) {
            // Nothing to do since quantity sync is disabled.
            return $this;
        }

        $b1 = new B1(['apiKey' => $apiKey, 'privateKey' => $privateKey]);
        do {
            $iteration++;
            Mage::log("syncB1ProductQuantitiesWithShop :: Iteration #$iteration", Zend_Log::INFO, 'b1.log');
            try {
                $response = $response = $data = $b1->exec('shop/product/quantity/list', array("lid" => $lastId, "lmod" => $modAfterDate));
            } catch (B1Exception $e) {
                print_r($e->getMessage());
                print_r($e->getExtraData());
            }
            if ($response === false) {
                // Log response error.
                throw new B1CronException('Error syncing product quantities');
            }
            // Process response.
            $data = isset($response['data']) ? $response['data'] : array();
            foreach ($data as $i => $productQuantity) {
                $lastId = $productQuantity['id'];
                Mage::getModel('accounting/product')->updateQuantity($productQuantity);
            }
        } while (!empty($data) && $response && count($data) == $listSize && $iteration < $maxRequestsToApi);
        if (!$syncAll) {
            Mage::getConfig()->saveConfig('accounting/config/latest_product_quantity_sync_date', $executionDateTime)->cleanCache();
        }
        return $this;
    }

    private function syncShopOrdersWithB1()
    {
        $initialProductSyncDone = Mage::getStoreConfig('accounting/config/initial_product_sync_done');
        if (!$initialProductSyncDone) {
            Mage::log("syncShopOrdersWithB1 :: Initial product sync is not done yet.", Zend_Log::INFO, 'b1.log');
            return false;
        }
        $initialOrderSyncDone = Mage::getStoreConfig('accounting/config/initial_order_sync_done');
        if (!$initialOrderSyncDone) {
            Mage::log("syncShopOrdersWithB1 :: Initial order sync is not done yet.", Zend_Log::INFO, 'b1.log');
        }
        $writeoff = Mage::getStoreConfig('accounting/config/order_writeoff');
        $enableQuantitySync = Mage::getStoreConfig('accounting/config/enable_quantity_sync');
        $apiKey = Mage::getStoreConfig('accounting/config/api_key');
        $privateKey = Mage::getStoreConfig('accounting/config/private_key');

        $b1 = new B1(['apiKey' => $apiKey, 'privateKey' => $privateKey]);
        $orders = Mage::getModel('sales/order')->findAndLockForSync();
        foreach ($orders as $order) {
            $data = $order->getSyncData($writeoff);
            try {
                $result = $b1->exec('shop/order/add', $data);
                if ($result !== false) {
                    // Success.
                    $order->resetLock($result['data']['orderId']);
                    if ($enableQuantitySync && isset($result['data']['update'])) {
                        foreach ($result['data']['update'] as $productQuantity) {
                            Mage::getModel('accounting/product')->updateQuantity($productQuantity);
                        }
                    }
                }
            } catch (B1Exception $e) {
                $receivedData = json_decode($e->getExtraData()['received']);
                if ($receivedData->code == 409) {
                    $order->resetLock($receivedData->data->orderId);
                } else {
                    ModelB1Orders::addFailedOrderSync($item->id);
                    $order->resetLock(null);
                }
                print_r($e->getMessage());
                print_r($e->getExtraData());
                throw new B1CronException('Error syncing order #' . $item->id . ' with B1.lt');
            }
        }
        if (!$initialOrderSyncDone) {
            Mage::getConfig()->saveConfig('accounting/config/initial_order_sync_done', 1)->cleanCache();
        }
        return $this;
    }

}
