<?php

class B1_Accounting_Model_Mysql4_Product extends Mage_Core_Model_Mysql4_Abstract
{

    public function _construct()
    {
        $this->_init('accounting/product', 'id');
        $this->_isPkAutoIncrement = false;
    }

    public function updateQuantity($product)
    {
        $id = (int)$product['id'];
        $quantity = (int)$product['quantity'];
        $this->_getWriteAdapter()->query("UPDATE {$this->getMainTable()} SET `quantity`=:q WHERE `id`=:id", array(
            'q' => $quantity,
            'id' => $id,
        ));
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('cataloginventory/stock_item')} s LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON s.`product_id` = p.`entity_id` SET s.`qty` = :q, s.`is_in_stock` = :s WHERE p.`b1_reference_id`=:id", array(
            'q' => $quantity,
            's' => $quantity > 0 ? 1 : 0,
            'id' => $id,
        ));
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('cataloginventory/stock_status')} s LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON s.`product_id` = p.`entity_id` SET s.`qty` = :q, s.`stock_status` = :s WHERE p.`b1_reference_id`=:id", array(
            'q' => $quantity,
            's' => $quantity > 0 ? 1 : 0,
            'id' => $id,
        ));
        return $this;
    }

    public function markAllAsGhosts()
    {
        $this->_getWriteAdapter()->query("UPDATE {$this->getMainTable()} SET `is_ghost`='1'");
        return $this;
    }

    public function unlinkAllGhosts()
    {
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('catalog/product')} p LEFT OUTER JOIN {$this->getMainTable()} b1p ON b1p.`id` = p.`b1_reference_id` SET p.`b1_reference_id` = NULL WHERE b1p.`is_ghost` = '1'");
        return $this;
    }

    public function deleteGhosts()
    {
        $this->_getWriteAdapter()->query("DELETE FROM {$this->getMainTable()} WHERE `is_ghost` = '1'");
        return $this;
    }

    public function syncQuantitiesWithShop()
    {
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('cataloginventory/stock_item')} s LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON s.`product_id` = p.`entity_id` LEFT OUTER JOIN {$this->getMainTable()} b1p ON b1p.`id` = p.`b1_reference_id` SET s.`qty` = b1p.`quantity`, s.`is_in_stock` = IF(b1p.`quantity` > 0, 1, 0) WHERE p.`b1_reference_id` IS NOT NULL");
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('cataloginventory/stock_status')} s LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON s.`product_id` = p.`entity_id` LEFT OUTER JOIN {$this->getMainTable()} b1p ON b1p.`id` = p.`b1_reference_id` SET s.`qty` = b1p.`quantity`, s.`stock_status` = IF(b1p.`quantity` > 0, 1, 0) WHERE p.`b1_reference_id` IS NOT NULL");
        return $this;
    }

}