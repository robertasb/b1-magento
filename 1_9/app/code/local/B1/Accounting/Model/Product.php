<?php

class B1_Accounting_Model_Product extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('accounting/product');
    }

    public function import($product)
    {
        $this->load($product['id']);
        $this->setData('id', $product['id']);
        $this->setData('name', $product['name']);
        $this->setData('code', $product['code']);
        $this->setData('quantity', $product['quantity']);
        $this->setData('is_product', $product['isProduct']);
        $this->setData('is_ghost', 0);
        $this->save();
        return $this;
    }

    public function updateQuantity($product)
    {
        $this->getResource()->updateQuantity($product);
        return $this;
    }

    public function markAllAsGhosts()
    {
        $this->getResource()->markAllAsGhosts();
        return $this;
    }

    public function unlinkAllGhosts()
    {
        $this->getResource()->unlinkAllGhosts();
        return $this;
    }

    public function deleteGhosts()
    {
        $this->getResource()->deleteGhosts();
        return $this;
    }

    public function syncQuantitiesWithShop()
    {
        $this->getResource()->syncQuantitiesWithShop();
        return $this;
    }

}