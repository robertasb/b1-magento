<?php

require_once('Mage/Adminhtml/Block/Widget/Grid/Column/Renderer/Input.php');

class B1_Accounting_Block_Adminhtml_Widget_Grid_Column_Renderer_Inline extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Input
{

    public function render(Varien_Object $row)
    {
        $collection = Mage::getModel('accounting/product')->getCollection();
        $html = '<select name="' . $this->getColumn()->getId() . '" class="select ' . $this->getColumn()->getInlineCss() . '" style="max-width: 150px;" onchange="updateB1ReferenceField(this, ' . $row->getId() . '); return false">';
        $html .= '<option></option>';
        foreach ($collection as $product) {
            $html .= '<option value="' . $product->getId() . '">' . $product->getName() . ' (' . $product->getCode() . ')</option>';
        }
        $html .= '</select>';
        return $html;
    }

}