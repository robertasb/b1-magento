<?php

class B1_Accounting_Block_Adminhtml_UnlinkedProducts extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_unlinkedProducts';
        $this->_blockGroup = 'accounting';
        $this->_headerText = Mage::helper('accounting')->__('Unlinked Products');
        parent::__construct();
        $this->_removeButton('add');
    }
}