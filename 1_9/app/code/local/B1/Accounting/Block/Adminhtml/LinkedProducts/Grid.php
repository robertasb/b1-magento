<?php

class B1_Accounting_Block_Adminhtml_LinkedProducts_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('linkedProductGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('linked_product_filter');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('type_id')
            ->addAttributeToSelect('b1_reference_id')
            ->addAttributeToSelect('b1_name')
            ->addAttributeToFilter('b1_reference_id', array(
                array('notnull' => true),
                array('neq' => 0),
            ))
            ->joinTable(array('b1p' => 'accounting/product'), 'id=b1_reference_id', array(
                'b1_name' => 'name',
                'b1_code' => 'code',
            ));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id',
            array(
                'header' => Mage::helper('accounting')->__('ID'),
                'width' => '50px',
                'type' => 'number',
                'index' => 'entity_id',
            ));
        $this->addColumn('b1_reference_id',
            array(
                'header' => Mage::helper('accounting')->__('B1 reference ID'),
                'width' => '50px',
                'type' => 'number',
                'index' => 'b1_reference_id',
            ));
        $this->addColumn('name',
            array(
                'header' => Mage::helper('accounting')->__('Name'),
                'index' => 'name',
            ));
        $this->addColumn('type',
            array(
                'header' => Mage::helper('accounting')->__('Type'),
                'width' => '60px',
                'index' => 'type_id',
                'type' => 'options',
                'options' => Mage::getSingleton('catalog/product_type')->getOptionArray(),
            ));
        $this->addColumn('sku',
            array(
                'header' => Mage::helper('accounting')->__('SKU'),
                'width' => '80px',
                'index' => 'sku',
            ));
        $this->addColumn('b1_name',
            array(
                'header' => Mage::helper('accounting')->__('B1 Name'),
                'index' => 'b1_name',
            ));
        $this->addColumn('b1_code',
            array(
                'header' => Mage::helper('accounting')->__('B1 Code'),
                'width' => '80px',
                'index' => 'b1_code',
            ));
        $this->addColumn('action',
            array(
                'header' => Mage::helper('accounting')->__('Action'),
                'width' => '50px',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('accounting')->__('Unlink'),
                        'url' => array(
                            'base' => '*/*/unlink'
                        ),
                        'field' => 'id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'actions',
            ));
        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/linkedProductsGrid', array('_current' => true));
    }

    public function getRowUrl($row)
    {
        return false;
    }

}