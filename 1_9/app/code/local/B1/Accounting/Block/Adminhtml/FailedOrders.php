<?php

class B1_Accounting_Block_Adminhtml_FailedOrders extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_failedOrders';
        $this->_blockGroup = 'accounting';
        $this->_headerText = Mage::helper('accounting')->__('Failed Orders');
        parent::__construct();
        $this->_removeButton('add');
    }
}