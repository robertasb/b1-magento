<?php

class B1_Accounting_Block_Adminhtml_LinkedProducts extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_linkedProducts';
        $this->_blockGroup = 'accounting';
        $this->_headerText = Mage::helper('accounting')->__('Linked Products');
        parent::__construct();
        $this->_removeButton('add');
    }
}