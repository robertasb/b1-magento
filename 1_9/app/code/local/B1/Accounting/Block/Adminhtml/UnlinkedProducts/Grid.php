<?php

class B1_Accounting_Block_Adminhtml_UnlinkedProducts_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('unlinkedProductGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('unlinked_product_filter');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('type_id')
            ->addAttributeToSelect('b1_reference_id')
            ->addAttributeToFilter('b1_reference_id', array(
                array('null' => true),
                array('eq' => 0),
            ), 'left');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id',
            array(
                'header' => Mage::helper('accounting')->__('ID'),
                'width' => '50px',
                'type' => 'number',
                'index' => 'entity_id',
            ));
        $this->addColumn('b1_reference_id',
            array(
                'header' => Mage::helper('accounting')->__('B1 reference ID'),
                'width' => '50px',
                'type' => 'number',
                'renderer' => 'accounting/adminhtml_widget_grid_column_renderer_inline',
                'index' => 'b1_reference_id',
            ));
        $this->addColumn('name',
            array(
                'header' => Mage::helper('accounting')->__('Name'),
                'index' => 'name',
            ));
        $this->addColumn('type',
            array(
                'header' => Mage::helper('accounting')->__('Type'),
                'width' => '60px',
                'index' => 'type_id',
                'type' => 'options',
                'options' => Mage::getSingleton('catalog/product_type')->getOptionArray(),
            ));
        $this->addColumn('sku',
            array(
                'header' => Mage::helper('accounting')->__('SKU'),
                'width' => '80px',
                'index' => 'sku',
            ));
        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/unlinkedProductsGrid', array('_current' => true));
    }

    public function getRowUrl($row)
    {
        return false;
    }

}