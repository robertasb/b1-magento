<?php

$installer = $this;
$setup = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('b1_product')};
CREATE TABLE {$this->getTable('b1_product')} (
  `id` INT(11) NOT NULL,
  `date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` TEXT,
  `code` TEXT,
  `quantity` INT(11) DEFAULT NULL,
  `is_product` TINYINT(1) DEFAULT NULL,
  `is_ghost` TINYINT(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$result = $installer->getConnection()->fetchOne("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{$this->getTable('catalog_product_entity')}' AND COLUMN_NAME = 'b1_reference_id'");
if (!$result) {
    $installer->run("ALTER TABLE {$this->getTable('catalog_product_entity')} ADD `b1_reference_id` INT(10) UNSIGNED NULL DEFAULT NULL, ADD INDEX (`b1_reference_id`);");
}
$result = $installer->getConnection()->fetchOne("SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{$this->getTable('sales_flat_order')}' AND COLUMN_NAME = 'b1_reference_id'");
if (!$result) {
    $installer->run("ALTER TABLE {$this->getTable('sales_flat_order')} ADD `b1_reference_id` INT(10) UNSIGNED NULL DEFAULT NULL, ADD `b1_sync_count` SMALLINT UNSIGNED NOT NULL DEFAULT 0, ADD `b1_sync_id` INT(11) NULL;");
}

$setup->addAttribute('catalog_product', 'b1_reference_id', array(
    'group' => 'B1',
    'input' => 'text',
    'type' => 'static',
    'label' => 'B1 Reference ID',
    'required' => 0,
    'user_defined' => 1,
    'default' => null,
));

$installer->endSetup();