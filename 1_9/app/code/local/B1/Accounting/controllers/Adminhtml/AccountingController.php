<?php
require_once(Mage::getBaseDir('lib') . '/B1/B1.php');

class B1_Accounting_Adminhtml_AccountingController extends Mage_Adminhtml_Controller_Action
{

    public function unlinkedProductsAction()
    {
        $this->addInfoMessages();
        $this->loadLayout()
            ->_setActiveMenu('accounting/unlinkedProducts');
        $this->_addContent(
            $this->getLayout()->createBlock('accounting/adminhtml_unlinkedProducts')
        );
        $this->renderLayout();
    }

    public function linkedProductsAction()
    {
        $this->addInfoMessages();
        $this->loadLayout()
            ->_setActiveMenu('accounting/linkedProducts');
        $this->_addContent(
            $this->getLayout()->createBlock('accounting/adminhtml_linkedProducts')
        );
        $this->renderLayout();
    }

    public function failedOrdersAction()
    {
        $this->addInfoMessages();
        $this->loadLayout()
            ->_setActiveMenu('accounting/failedOrders');
        $this->_addContent(
            $this->getLayout()->createBlock('accounting/adminhtml_failedOrders')
        );
        $this->renderLayout();
    }

    public function linkAction()
    {
        $productId = (int)$this->getRequest()->getParam('id');
        $referenceId = (int)$this->getRequest()->getParam('referenceId');
        if ($productId && $referenceId) {
            try {
                $model = $this->loadProductModel($productId);
                $model->updateB1ReferenceId($referenceId);
                $this->respond(1);
            } catch (Exception $ex) {
                Mage::logException($ex);
                $this->respond(0, $ex->getMessage());
            }
        } else {
            $this->respond(0, 'ID/Reference ID is missing.');
        }
    }

    public function unlinkAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                Mage::getModel('catalog/product')->setId($this->getRequest()->getParam('id'))->removeB1ReferenceId();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Product was successfully unlinked.'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/linkedProducts');
    }

    public function resetAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                Mage::getModel('sales/order')->setId($this->getRequest()->getParam('id'))->resetB1SyncCount();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Reset successfully finished. Order will be synced next time order sync cron runs.'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/failedOrders');
    }

    public function unlinkedProductsGridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('accounting/adminhtml_unlinkedProducts_grid')->toHtml()
        );
    }

    public function linkedProductsGridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('accounting/adminhtml_linkedProducts_grid')->toHtml()
        );
    }

    public function failedOrdersGridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('accounting/adminhtml_failedOrders_grid')->toHtml()
        );
    }

    private function loadProductModel($productId)
    {
        $model = Mage::getModel('catalog/product')->load($productId);
        if (!$model->getId()) {
            throw new Exception("No product with specified ID found.");
        }
        return $model;
    }

    private function respond($status, $errorMessage = '')
    {
        echo json_encode(array(
            'status' => $status,
            'errorMessage' => $errorMessage,
        ));
    }

    private function addInfoMessages()
    {
        Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('adminhtml')->__('- "Unlinked products" lists eshop products that are not linked with B1 products. Every new product entered in eshop or B1 should be manually linked here. To link products simply select product from the dropdown.'));
        Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('adminhtml')->__('- "Linked products" lists products that are linked between eshop & B1. To unlink simply find the required product in the table and press "Unlink" button.'));
        Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('adminhtml')->__('- "Orders failed to sync" lists all orders that failed to sync more than the maximum allowed times. Orders in this list will not be synced automatically with B1. You should inspect each order and try to figure out why sync failed, then reset the order, by pressing the reset button near the fixed order. To figure out what went wrong, by default, B1 keeps track of errors and various debug information in database, b1_log table.'));
        $initialProductSyncDone = Mage::getStoreConfig('accounting/config/initial_product_sync_done');
        if (!$initialProductSyncDone) {
            Mage::getSingleton('adminhtml/session')->addWarning(Mage::helper('adminhtml')->__('Initial sync is in progress. Please wait while products are being fetched from B1.'));
        }
    }

}