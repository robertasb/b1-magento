<?php

require_once('Mage/Sales/Model/Resource/Order.php');

class B1_Sales_Model_Resource_Order extends Mage_Sales_Model_Resource_Order
{

    public function resetB1SyncCount($orderId)
    {
        $orderId = (int)$orderId;
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('sales/order')} SET `b1_sync_count`= 0 WHERE `entity_id`=:id", array(
            'id' => $orderId,
        ));
    }

    public function resetStuck()
    {
        $sql = "UPDATE {$this->getMainTable()} SET `b1_sync_id` = NULL WHERE `b1_sync_id` <= :t AND `b1_reference_id` IS NULL";
        $this->_getWriteAdapter()->query($sql, array(
            't' => time() - 3600 //86400
        ));
        return $this;
    }

    public function findAndLockForSync()
    {
        $statusName = Mage::getStoreConfig('accounting/config/order_status_name_check');
        $maxSyncCount = Mage::getStoreConfig('accounting/config/order_max_sync_count');
        $orderSyncFrom = Mage::getStoreConfig('accounting/config/order_sync_from');
        $syncId = time();
        $sql = "UPDATE {$this->getMainTable()} SET `b1_sync_id`=:s WHERE `b1_reference_id` IS NULL AND `b1_sync_count`<:c AND `created_at`>=:f AND (status=:os OR entity_id IN (SELECT parent_id FROM {$this->getTable('sales/order_status_history')} WHERE status=:os))";
        $this->_getWriteAdapter()->query($sql, array(
            's' => $syncId,
            'c' => $maxSyncCount,
            'os' => $statusName,
            'f' => $orderSyncFrom,
        ));
        $orders = Mage::getModel('sales/order')
            ->getCollection()
            ->addFieldToFilter('b1_sync_id', array('eq' => $syncId));
        return $orders;
    }

    public function resetLock($orderId, $referenceId)
    {
        if ($referenceId == null) {
            $sql = "UPDATE {$this->getMainTable()} SET `b1_sync_count` = `b1_sync_count`+1, `b1_sync_id` = NULL, `b1_reference_id` = NULL WHERE `entity_id` = :o";
            $this->_getWriteAdapter()->query($sql, array(
                'o' => $orderId,
            ));
        } else {
            $sql = "UPDATE {$this->getMainTable()} SET `b1_sync_count` = `b1_sync_count`+1, `b1_sync_id` = NULL, `b1_reference_id` = :r WHERE `entity_id` = :o";
            $this->_getWriteAdapter()->query($sql, array(
                'r' => $referenceId,
                'o' => $orderId,
            ));
        }
        return $this;
    }

}