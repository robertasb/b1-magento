<?php

require_once('Mage/Sales/Model/Order.php');

class B1_Sales_Model_Order extends Mage_Sales_Model_Order
{

    public function resetB1SyncCount()
    {
        $this->getResource()->resetB1SyncCount($this->getId());
        return $this;
    }

    public function resetStuck()
    {
        $this->getResource()->resetStuck();
        return $this;
    }

    public function findAndLockForSync()
    {
        return $this->getResource()->findAndLockForSync();
    }

    public function getSyncData($writeoff = true)
    {
        $shopId = Mage::getStoreConfig('accounting/config/shop_id');
        $mask = Mage::getStoreConfig('accounting/config/invoice_product_name_mask');
        $items = $this->getAllVisibleItems();
        $billingAddress = $this->getBillingAddress();
        $shippingAddress = $this->getShippingAddress();
        $firstItem = $items[0];
        $data = array(
            'prefix' => $shopId,
            'orderid' => $this->getId(),
            'orderdate' => date('Y-m-d', strtotime($this->getCreatedAt())),
            'orderno' => $this->getId(),
            'currency' => $this->getOrderCurrencyCode(),
            'shippingAmount' => $this->convertFloatToInt($this->getShippingAmount() + $this->getShippingTaxAmount()),
            'discount' => $this->convertFloatToInt($this->getDiscountAmount()),
            'total' => $this->convertFloatToInt($this->getGrandTotal()),
            'orderemail' => $this->getCustomerEmail(),
            'vatrate' => intval(round($firstItem->getTaxPercent())),
            'writeoff' => $writeoff ? 1 : 0,
            'billing' => array(
                'name' => $billingAddress->getName(),
                'iscompany' => $billingAddress->getCompany() != '' ? 1 : 0,
                'vatcode' => $billingAddress->getVatId(),
                'address' => $billingAddress->getStreet1(),
                'city' => $billingAddress->getCity(),
                'postcode' => $billingAddress->getPostcode(),
                'country' => $billingAddress->getCountryId(),
            ),
            'delivery' => array(
                'name' => $shippingAddress->getName(),
                'iscompany' => $shippingAddress->getCompany() != '' ? 1 : 0,
                'address' => $shippingAddress->getStreet1(),
                'city' => $shippingAddress->getCity(),
                'postcode' => $shippingAddress->getPostcode(),
                'country' => $shippingAddress->getCountryId(),
            ),
        );
        foreach ($items as $i => $item) {
            $name = str_replace(array('{name}', '{model}', '{sku}', '{upc}', '{ean}', '{jan}', '{isbn}', '{mpn}'), array($item->getName(), 'MODEL_NOT_SUPPORTED', $item->getSku(), 'UPC_NOT_SUPPORTED', 'EAN_NOT_SUPPORTED', 'JAN_NOT_SUPPORTED', 'ISBN_NOT_SUPPORTED', 'MPN_NOT_SUPPORTED'), $mask);
            $product = $item->getProduct();
            $data['items'][$i] = array(
                'id' => $product['b1_reference_id'],
                'name' => $name,
                'quantity' => $this->convertFloatToInt($item->getQtyOrdered()),
                'price' => $this->convertFloatToInt($item->getPriceInclTax()),
                'sum' => $this->convertFloatToInt($item->getRowTotalInclTax()),
                'vatrate' => intval(round($item->getTaxPercent())),
            );
        }
        return $data;
    }

    public function resetLock($referenceId)
    {
        $this->getResource()->resetLock($this->getId(), $referenceId);
        return $this;
    }

    private function convertFloatToInt($value)
    {
        return intval(round($value * 100));
    }

}