<?php
require_once('Mage/Catalog/Model/Product.php');

class B1_Catalog_Model_Product extends Mage_Catalog_Model_Product
{

    public function removeB1ReferenceId()
    {
        $this->getResource()->removeB1ReferenceId($this->getId());
        return $this;
    }

    public function updateB1ReferenceId($referenceId)
    {
        $this->getResource()->updateB1ReferenceId($this->getId(), $referenceId);
        return $this;
    }

    public function updateQuantity($product)
    {
        $this->getResource()->updateQuantity($product);
        return $this;
    }

    public function markAllAsGhosts()
    {
        $this->getResource()->markAllAsGhosts();
        return $this;
    }

    public function unlinkAllGhosts()
    {
        $this->getResource()->unlinkAllGhosts();
        return $this;
    }

    public function deleteGhosts()
    {
        $this->getResource()->deleteGhosts();
        return $this;
    }

    public function syncQuantitiesWithShop()
    {
        $this->getResource()->syncQuantitiesWithShop();
        return $this;
    }

}