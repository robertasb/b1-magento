<?php
require_once('Mage/Catalog/Model/Resource/Product.php');

class B1_Catalog_Model_Resource_Product extends Mage_Catalog_Model_Resource_Product
{

    public function removeB1ReferenceId($productId)
    {
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('catalog/product')} SET `b1_reference_id`= NULL WHERE `entity_id`=:id", array(
            'id' => $productId,
        ));
    }

    public function updateB1ReferenceId($productId, $referenceId)
    {
        $referenceId = (int)$referenceId;
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('catalog/product')} SET `b1_reference_id`=:r WHERE `entity_id`=:id", array(
            'r' => $referenceId,
            'id' => $productId,
        ));
        return $this;
    }

    public function updateQuantity($product)
    {
        $id = (int)$product['id'];
        $quantity = (int)$product['quantity'];
        $this->_getWriteAdapter()->query("UPDATE {$this->getMainTable()} SET `quantity`=:q WHERE `id`=:id", array(
            'q' => $quantity,
            'id' => $id,
        ));
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('cataloginventory/stock_item')} s LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON s.`product_id` = p.`entity_id` SET s.`qty` = :q WHERE p.`b1_reference_id`=:id", array(
            'q' => $quantity,
            'id' => $id,
        ));
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('cataloginventory/stock_status')} s LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON s.`product_id` = p.`entity_id` SET s.`qty` = :q WHERE p.`b1_reference_id`=:id", array(
            'q' => $quantity,
            'id' => $id,
        ));
        return $this;
    }

    public function markAllAsGhosts()
    {
        $this->_getWriteAdapter()->query("UPDATE {$this->getMainTable()} SET `is_ghost`='1'");
        return $this;
    }

    public function unlinkAllGhosts()
    {
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('catalog/product')} p LEFT OUTER JOIN {$this->getMainTable()} b1p ON b1p.`id` = p.`b1_reference_id` SET p.`b1_reference_id` = NULL WHERE b1p.`is_ghost` = '1'");
        return $this;
    }

    public function deleteGhosts()
    {
        $this->_getWriteAdapter()->query("DELETE FROM {$this->getMainTable()} WHERE `is_ghost` = '1'");
        return $this;
    }

    public function syncQuantitiesWithShop()
    {
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('cataloginventory/stock_item')} s LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON s.`product_id` = p.`entity_id` LEFT OUTER JOIN {$this->getMainTable()} b1p ON b1p.`id` = p.`b1_reference_id` SET s.`qty` = b1p.`quantity` WHERE p.`b1_reference_id` IS NOT NULL");
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('cataloginventory/stock_status')} s LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON s.`product_id` = p.`entity_id` LEFT OUTER JOIN {$this->getMainTable()} b1p ON b1p.`id` = p.`b1_reference_id` SET s.`qty` = b1p.`quantity` WHERE p.`b1_reference_id` IS NOT NULL");
        return $this;
    }

}