# LT #

Įskiepis skirtas sinchronizuoti produktus tarp Magento ir B1.lt aplikacijos.

### Reikalavimai ###

* PHP 5.5
* Magento 1.9.2.0

### Diegimas ###

* SVARBU! Perskaitykite [dokumentacija](https://www.b1.lt/help/e-shop-module-settings) ir atlikite nurodytus ten veiksmus.
* [NEBŪTINA] Pasidarykite failų atsarginę kopiją.
* Padaryti atsarginę DB kopiją.
* Išpakuokite archyvą. Atidarykite "/app/code/local/B1/Accounting/etc/config.xml". Raskite <order_sync_from>2015-01-01 00:00:00</order_sync_from> ir pakeiskite nuo kokios datos bus perduodami užsakymai į B1. Išsaugokite. Pvz: Tik užsakymai kurie buvo sukurti po "2015-01-01 00:00:00" bus sinchronizuojami su B1.
* Perkelkite failus į magento direktoriją.
* Administracijos skiltyje atidarykite "System/Configuration/B1 plugin/Configuration settings" ir suveskite reikiamą informaciją. Pastaba: Jeigu nerandate "B1 plugin" punkto - pabandykite išvalyti magento cache'a (System/Cache management/Clear magento cache) ir prisijungti iš naujo.
* Nustatykite cron (System/Configuration/B1 plugin/Configuration settings/CRON rules) vykdymo laikus. Pastaba: Magento cron vykdytojas (System/Configuration/Advanced/System/Cron (Scheduled Tasks) - all the times are in minutes) gali ne iš karto sugeneruoti darbus.
* Cron darbai (Sync Orders, Fetch all products) turi susinchronizuoti užsakymus ir gauti prekės iš B1.
* Paspauskite "B1 Accounting/Unlinked products" ir susiekite produktus.

### Pastabos ###

* Log failai:
    * /var/log/b1.log
    * /var/log/exceptions.log
* Į log failus plugin'as rašo tik jeigu jam tai leista daryti (System/Configuration/Advanced/Developer/Log settings; Enabled=true)

### Kontaktai ###

* Kilus klausimams, prašome kreiptis info@b1.lt



# EN #

Plugin to sync products and sales between Magento and the B1.lt application.

### Requirements ###

* PHP 5.5
* Magento 1.9.2.0

### Installation ###

* IMPORTANT! Read the [documentation](https://www.b1.lt/help/e-shop-module-settings/) and do exactly what it tells you.
* [OPTIONAL] Backup files.
* Backup DB.
* Extract files from the archive. Open "/app/code/local/B1/Accounting/etc/config.xml". Find <order_sync_from>2015-01-01 00:00:00</order_sync_from> and change the date. This date specifies from when to start syncing orders with B1. Save the file. Example: Only orders that were created after "2015-01-01 00:00:00" will be sent to B1.
* Copy files to magento directory.
* In admin panel open "System/Configuration/B1 plugin/Configuration settings" and enter required information. Note: If there is no "B1 plugin" in left menu - try clearing magento cache (System/Cache management/Clear magento cache) and relogging.
* Configure cron (System/Configuration/B1 plugin/Configuration settings/CRON rules) jobs execution times. Note: Magento's internal cron job runner (System/Configuration/Advanced/System/Cron (Scheduled Tasks) - all the times are in minutes) may not regenerate the queue for some time.
* On next run, cron jobs (Sync Orders, Fetch all products) will sync orders and fetch products from B1.
* Select "B1 Accounting/Unlinked products" and link items.

### Notes ###

* Log files:
    * /var/log/b1.log
    * /var/log/exceptions.log
* Plugin writes to log only when it is allowed (System/Configuration/Advanced/Developer/Log settings; Enabled=true)

### Contacts ###

* For any questions, please contact info@b1.lt