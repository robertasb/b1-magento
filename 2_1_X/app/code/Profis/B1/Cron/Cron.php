<?php

namespace Profis\B1\Cron;

class Cron
{

    const TTL = 3600;
    const MAX_ITERATIONS = 300;
    const ORDERS_PER_ITERATION = 300;
    const ORDER_SYNC_THRESHOLD = 10;
    const PLUGIN_NAME = 'Magento';

    protected $_logger;
    protected $_settings;
    protected $_clients;
    protected $_items;
    protected $_helper;
    protected $_objectManager;
    protected $_library;

    public function __construct()
    {
        $this->_items = \Magento\Framework\App\ObjectManager::getInstance()->get('\Profis\B1\Model\Items');
        $this->_helper = \Magento\Framework\App\ObjectManager::getInstance()->get('\Profis\B1\Model\Helper');
        $this->_settings = \Magento\Framework\App\ObjectManager::getInstance()->get('\Profis\B1\Model\Settings');
        $this->_clients = \Magento\Framework\App\ObjectManager::getInstance()->get('\Profis\B1\Model\Clients');
        $this->_orders = \Magento\Framework\App\ObjectManager::getInstance()->get('\Profis\B1\Model\Orders');
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        set_time_limit(self::TTL);
        ini_set('max_execution_time', self::TTL);

        $this->_library = new \Profis\B1\lib\B1\B1(['apiKey' => $this->_settings->getValue('b1/api_key'), 'privateKey' => $this->_settings->getValue('b1/private_key')]);
    }

    public function products()
    {
        $i = 0;
        $lid = 0;
        $b1_ids = array();
        if ($this->_settings->getValue('b1/products_next_sync') < date('Y-m-d H:i:s') || $this->_settings->getValue('b1/products_next_sync') == '') {
            if ($this->_settings->getValue('b1/products_next_sync') != '') {
                $this->_settings->setValue('b1/products_next_sync', '');
                $this->_settings->setValue('b1/products_sync_count', 0);
            }
            do {
                $productsSyncCount = $this->_settings->getValue('b1/products_sync_count');
                ob_start();
                $i++;
                try {
                    $data = $this->_library->exec('shop/product/list', array("lid" => $lid, "pluginName" => self::PLUGIN_NAME));
                    if ($data != false) {
                        foreach ($data['data'] as $item) {
                            $b1_ids[] = htmlspecialchars($item['id']);
                            $this->_items->addItem($item['name'], $item['code'], $item['id']);
                            if ($this->_settings->getValue('b1/relation_type') == '1_1') {
                                if ($item['quantity']) {
                                    $post_data = $this->_items->getB1Item($item['id']);
                                    if (count($post_data) != 0) {
                                        $this->_items->quantityUpdate($item['quantity'], $post_data[0]->shop_product_id);
                                    }
                                }
                            }
                        }
                        if (count($data['data']) == 100) {
                            $lid = $data['data'][99]['id'];
                        } else {
                            $lid = -1;
                        }
                    } else {
                        $this->_settings->setValue('b1/products_sync_count', $productsSyncCount + 1);
                        throw new \Profis\B1\Cron\B1CronException('Error getting data from B1.lt');
                    }
                } catch (\Profis\B1\lib\B1\B1Exception $e) {
                    $this->_settings->setValue('b1/products_sync_count', $productsSyncCount + 1);
                    print_r($e->getMessage());
                    print_r($e->getExtraData());
                }
                printf("%u", $i);
                ob_end_flush();
            } while ($lid != -1 && $i < self::MAX_ITERATIONS && $productsSyncCount < 9);

            if ($this->_settings->getValue('b1/products_sync_count') >= 10) {
                $this->_settings->setValue('b1/products_next_sync', date('Y-m-d H:i:s', strtotime('6 hour')));
            }

            if (!empty($b1_ids)) {
                $this->_items->deleteLinkedItems($b1_ids);
                $this->_items->deleteB1Items($b1_ids);
            }
            printf("%u", 'OK');
        }

        return $this;
    }

    public function quantities()
    {
        if ($this->_settings->getValue('b1/relation_type') == '1_1') {
            printf("%u", 'OK');
            return false;
        }
        $i = 0;
        $lid = 0;
        $b1_ids = array();

        if ($this->_settings->getValue('b1/quantities_sync_count') >= 10 && $this->_settings->getValue('b1/quantities_next_sync') == '') {
            $this->_settings->setValue('b1/quantities_next_sync', date('Y-m-d H:i:s', strtotime('6 hour')));
        }
        if ($this->_settings->getValue('b1/quantities_next_sync') < date('Y-m-d H:i:s') || $this->_settings->getValue('b1/quantities_next_sync') == '') {
            if ($this->_settings->getValue('b1/quantities_next_sync') != '') {
                $this->_settings->setValue('b1/quantities_next_sync', '');
                $this->_settings->setValue('b1/quantities_sync_count', 0);
            }
            do {
                $quantitiesSyncCount = $this->_settings->getValue('b1/quantities_sync_count');
                ob_start();
                $i++;
                try {
                    $data = $this->_library->exec('shop/product/quantity/list', array("lid" => $lid, "pluginName" => self::PLUGIN_NAME));
                    if ($data != false) {
                        foreach ($data['data'] as $item) {
                            $b1_ids[] = htmlspecialchars($item['id']);
                            if ($item['quantity']) {
                                $post_data = $this->_items->getB1Item($item['id']);
                                if (count($post_data) != 0) {
                                    $this->_items->quantityUpdate($item['quantity'], $post_data[0]->shop_product_id);
                                }
                            }
                        }
                        if (count($data['data']) == 100) {
                            $lid = $data['data'][99]['id'];
                        } else {
                            $lid = -1;
                        }
                    } else {
                        $this->_settings->setValue('b1/quantities_sync_count', $quantitiesSyncCount + 1);
                        throw new \Profis\B1\Cron\B1CronException('Error getting data from B1.lt');
                    }
                } catch (\Profis\B1\lib\B1\B1Exception $e) {
                    $this->_settings->setValue('b1/quantities_sync_count', $quantitiesSyncCount + 1);
                    $this->_helper->printPre($e->getMessage());
                    $this->_helper->printPre($e->getExtraData());
                }
                printf("%u", $i);
                ob_end_flush();
            } while ($lid != -1 && $i < self::MAX_ITERATIONS && $quantitiesSyncCount < 9);

            if (!empty($b1_ids)) {
                $this->_items->deleteLinkedItems($b1_ids);
            }
        }
        printf("%u", 'OK');
        return $this;
    }

    public function orders()
    {
        $id = time();
        try {
            $orders_sync_from = $this->_settings->getValue('b1/orders_sync_from');
            if (!$orders_sync_from) {
                throw new \Profis\B1\Cron\B1CronException('Not set orders_sync_from value');
            }
            $data_prefix = $this->_settings->getValue('b1/shop_identifier');
            if (!$data_prefix) {
                throw new \Profis\B1\Cron\B1CronException('Not set shop_id value');
            }
            $initial_sync = $this->_settings->getValue('b1/initial_sync');
            if ($initial_sync == null) {
                throw new \Profis\B1\Cron\B1CronException('Not set initial_sync value');
            }
            $this->_orders->resetNotSyncB1Orders();
            $this->_orders->hideOrdersFromSyncByDate($orders_sync_from);
            $this->_orders->setSyncId($id, self::TTL, self::ORDER_SYNC_THRESHOLD);
            $i = 0;
            do {
                ob_start();
                $i++;
                $orders = $this->_orders->getSyncOrders($orders_sync_from, $id, self::ORDERS_PER_ITERATION);
                $processed = 0;
                foreach ($orders as $item) {
                    if ($item['b1_sync_id'] == null) {
                        $this->_orders->addB1Order($item['entity_id'], $id);
                    }
                    $order_data = $this->generateOrderData($item, $data_prefix, $initial_sync);
                    try {
                        $request = $this->_library->exec('shop/order/add', $order_data['data']);
                        if ($request != false) {
                            $this->_orders->setOrderReference($request['data']['orderId'], $item['entity_id']);
                            if (!isset($order_data['data']['billing']['refid'])) {
                                $this->_clients->addClient($request['data']['clientId'], $order_data['customer_id']);
                            }
                        } else {
                            $this->_orders->setFailedSync($item['entity_id']);
                        }
                    } catch (\Profis\B1\lib\B1\B1DuplicateException $e) {
                        $this->_orders->setFailedSync($item['entity_id']);
                        $receivedData = $e->getExtraData();
                        $this->_orders->setOrderReference($receivedData['data']['orderid'], $item['entity_id']);
                        $this->_helper->printPre($e->getMessage());
                        $this->_helper->printPre($e->getExtraData());
                        throw new \Profis\B1\Cron\B1CronException('Error syncing order #' . $item['entity_id'] . ' with B1.lt');
                    } catch (\Profis\B1\lib\B1\B1Exception $e) {
                        $this->_orders->setFailedSync($item['entity_id']);
                        $this->_helper->printPre($e->getMessage());
                        $this->_helper->printPre($e->getExtraData());
                        throw new \Profis\B1\Cron\B1CronException('Error syncing order #' . $item['entity_id'] . ' with B1.lt');
                    }
                    $processed++;
                    printf("%u", $i . '-' . $processed);
                }
                ob_end_flush();
            } while ($processed == self::ORDERS_PER_ITERATION && $i < self::MAX_ITERATIONS);
            if ($initial_sync == 0) {
                $this->_settings->setValue('b1/initial_sync', '1');
            }
            printf("%u", 'OK');
        } catch (\Profis\B1\Cron\B1CronException $e) {
            $this->_orders->unsetSyncOrders($id);
            $this->_helper->printPre($e->getMessage());
            $this->_helper->printPre($e->getExtraData());
        }
        return $this;
    }

    private function generateOrderData($item, $data_prefix, $initial_sync)
    {
        $tax_rate = $this->_settings->getValue('b1/tax_rate');
        $order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($item['entity_id']);
        $order_data = array();
        $client = $this->_clients->getB1Client($item['customer_id']);
        if (!empty($client)) {
            $order_data['billing']['refid'] = $client[0]['b1_client_id'];
        }
        $order_data['pluginName'] = self::PLUGIN_NAME;
        $order_data['prefix'] = $data_prefix;
        $order_data['writeoff'] = $initial_sync;
        $order_data['orderid'] = $item['entity_id'];
        $order_data['orderdate'] = substr($item['created_at'], 0, 10);
        $order_data['orderno'] = $item['entity_id'];
        $order_data['currency'] = $item['base_currency_code'];
        $order_data['discount'] = intval(round($order->getBaseDiscountAmount() * 100));
        $order_data['total'] = intval(round($order->getBaseSubtotalInclTax() * 100));
        $order_data['orderemail'] = $item['customer_email'];
        $order_data['vatRate'] = intval(round($tax_rate));
        $order_data['shippingamount'] = intval(round($order->getBaseShippingInclTax() * 100));
        $order_data['billing']['name'] = $order->getBillingAddress()->getCompany() == null ? $order->getBillingAddress()->getName() : $order->getBillingAddress()->getCompany();
        $order_data['billing']['iscompany'] = $order->getBillingAddress()->getCompany() == null ? 0 : 1;
        $order_data['billing']['address'] = $order->getBillingAddress()->getStreet()[0];
        $order_data['billing']['city'] = $order->getBillingAddress()->getCity();
        $order_data['billing']['postcode'] = $order->getBillingAddress()->getPostcode();
        $order_data['billing']['country'] = $order->getBillingAddress()->getCountryId();
        $order_data['delivery']['name'] = $order->getShippingAddress()->getCompany() == null ? $order->getShippingAddress()->getName() : $order->getShippingAddress()->getCompany();
        $order_data['delivery']['iscompany'] = $order->getShippingAddress()->getCompany() == null ? 0 : 1;
        $order_data['delivery']['address'] = $order->getShippingAddress()->getStreet()[0];
        $order_data['delivery']['city'] = $order->getShippingAddress()->getCity();
        $order_data['delivery']['country'] = $order->getShippingAddress()->getCountryId();
        if ($order_data['billing']['name'] == '') {
            $order_data['billing'] = $order_data['delivery'];
        }
        if ($order_data['delivery']['name'] == '') {
            $order_data['delivery'] = $order_data['billing'];
        }
        if ($order->getTotalItemCount() < 0) {
            throw new \Profis\B1\Cron\B1CronException('Not found order_products data, for #' . htmlspecialcharacters((int)$item['entity_id']) . ' order.', array('order_data' => $order_data));
        }
        foreach ($order->getAllVisibleItems() as $key => $product) {
            if ($this->_settings->getValue('b1/relation_type') == '1_1') {
                $b1_product = $this->_items->getB1ItemByShopId($product->getProductId());
                if (count($b1_product) > 0) {
                    $order_data['items'][$key]['id'] = $b1_product['0']['b1_product_id'];
                }
            }
            $order_data['items'][$key]['name'] = $product->getName();
            $order_data['items'][$key]['quantity'] = intval(round($product->getQtyOrdered(), 2) * 100);
            $order_data['items'][$key]['price'] = intval(round($product->getPriceInclTax(), 2) * 100);
            $order_data['items'][$key]['sum'] = intval($order_data['items'][$key]['price'] * round($product->getQtyOrdered(), 2));
        }
        return [
            'customer_id' => $item['customer_id'],
            'data' => $order_data
        ];
    }

}
