<?php

namespace Profis\B1\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        $connection = $installer->getConnection();
        $sql = "
        CREATE TABLE IF NOT EXISTS `b1_items` (
          `id` int(11) NOT NULL,
          `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `name` text,
          `code` text,
          `quantity` int(11) DEFAULT NULL,
          `is_product` tinyint(1) DEFAULT NULL,
          `is_ghost` tinyint(1) DEFAULT '0'
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $connection->query($sql);

        $sql = "ALTER TABLE `b1_items`
          ADD PRIMARY KEY (`id`),
          ADD UNIQUE KEY `id` (`id`);";
        $connection->query($sql);

        $sql = "CREATE TABLE `b1_linked_items` (
          `shop_product_id` int(11) NOT NULL,
          `b1_product_id` int(11) NOT NULL,
          `shop_product_name` text NOT NULL,
          `b1_product_name` text NOT NULL,
          `b1_product_code` text NOT NULL
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $connection->query($sql);

        $sql = "CREATE TABLE `b1_orders` (
          `b1_order_id` int(11) DEFAULT NULL,
          `shop_order_id` int(11) NOT NULL,
          `b1_sync_count` int(11) NOT NULL DEFAULT '0',
          `b1_sync_id` int(11) DEFAULT NULL,
          `next_sync` timestamp NULL DEFAULT NULL
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $connection->query($sql);

        $sql = "ALTER TABLE `b1_orders`
          ADD PRIMARY KEY (`shop_order_id`),
          ADD UNIQUE KEY `shop_order_id` (`shop_order_id`);";
        $connection->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `b1_clients` (
            `id` INTEGER(10) NOT NULL AUTO_INCREMENT,
            `b1_client_id` INTEGER(10) DEFAULT NULL,
            `shop_client_id` INTEGER(10) DEFAULT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `id` (`b1_client_id`, `shop_client_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $connection->query($sql);

        $sql = "INSERT INTO `core_config_data` (`config_id`, `scope`, `scope_id`, `path`, `value`) VALUES (NULL, 'default', 0, 'b1/initial_sync', '0');";
        $connection->query($sql);
        $sql = "INSERT INTO `core_config_data` (`config_id`, `scope`, `scope_id`, `path`, `value`) VALUES (NULL, 'default', 0, 'b1/relation_type', '1_1');";
        $connection->query($sql);
        $sql = "INSERT INTO `core_config_data` (`config_id`, `scope`, `scope_id`, `path`, `value`) VALUES (NULL, 'default', 0, 'b1/quantities_next_sync', '');";
        $connection->query($sql);
        $sql = "INSERT INTO `core_config_data` (`config_id`, `scope`, `scope_id`, `path`, `value`) VALUES (NULL, 'default', 0, 'b1/quantities_sync_count', '0');";
        $connection->query($sql);
        $sql = "INSERT INTO `core_config_data` (`config_id`, `scope`, `scope_id`, `path`, `value`) VALUES (NULL, 'default', 0, 'b1/products_next_sync', '');";
        $connection->query($sql);
        $sql = "INSERT INTO `core_config_data` (`config_id`, `scope`, `scope_id`, `path`, `value`) VALUES (NULL, 'default', 0, 'b1/products_sync_count', '0');";
        $connection->query($sql);

        $installer->endSetup();
    }

}
