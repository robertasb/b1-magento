<?php

namespace Profis\B1\Block\Adminhtml\Dashboard;

use Magento\Framework\View\Element\Template;

class Settings extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    private $_authSession;

    private $_modelSettings;
    private $_modelItems;
    private $_modelOrders;
    private $_config;
    protected $_isScopePrivate;

    public function __construct(
        \Magento\Backend\Model\Auth\Session $authSession,
        \Profis\B1\Model\Settings $modelSettings,
        \Profis\B1\Model\Items $modelItems,
        \Profis\B1\Model\Orders $modelOrders,
        \Magento\Framework\App\Config\MutableScopeConfigInterface $config,
        Template\Context $context,
        array $data = []
    )
    {
        $this->_authSession = $authSession;
        $this->_modelSettings = $modelSettings;
        $this->_modelItems = $modelItems;
        $this->_modelOrders = $modelOrders;
        $this->_config = $config;
        $this->_isScopePrivate = true;
        parent::__construct($context, $data);
    }

    public function settings()
    {
        return $this->_modelSettings;
    }

    public function items()
    {
        return $this->_modelItems;
    }

    public function orders()
    {
        return $this->_modelOrders;
    }


}
