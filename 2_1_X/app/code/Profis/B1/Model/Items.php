<?php

namespace Profis\B1\Model;

use Items\Framework\App\Config\Value;

class Items extends \Magento\Framework\App\Config\Value
{

    /**
     * @var string
     */
    public $configPath = "advanced/modules_disable_output/Profis_B1";

    /**
     * @var \Magento\Framework\Model\Context
     */
    private $_context;

    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    private $_resourceConfig;
    private $_connection;
    private $_tables;
    protected $_config;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\MutableScopeConfigInterface $_config,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {

        $this->_context = $context;
        $this->_config = $_config;
        $this->_resourceConfig = $resourceConfig;
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $this->_connection = $this->_resources->getConnection();
        $this->_tables = [];
        $this->_tables['catalog_product_entity'] = $this->_resources->getTableName('catalog_product_entity');
        $this->_tables['catalog_product_entity_varchar'] = $this->_resources->getTableName('catalog_product_entity_varchar');
        $this->_tables['b1_items'] = $this->_resources->getTableName('b1_items');
        $this->_tables['b1_linked_items'] = $this->_resources->getTableName('b1_linked_items');
        $this->_tables['cataloginventory_stock_item'] = $this->_resources->getTableName('cataloginventory_stock_item');

        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * @return \Magento\Framework\App\Config\Value
     */
    public function save()
    {
        $this->_disabledModule(!$this->getValue());

        return parent::save();
    }

    /**
     * @param $disable
     */
    private function _disabledModule($disable)
    {
        $this->_resourceConfig->saveConfig($this->configPath, $disable, $this->getScope(), $this->getScopeId());
    }

    public function getUnlinkedItems($from, $items)
    {
        $sql = "SELECT * FROM " . $this->_tables['catalog_product_entity'] . "
                LEFT JOIN " . $this->_tables['catalog_product_entity_varchar'] . " ON " . $this->_tables['catalog_product_entity'] . ".entity_id = " . $this->_tables['catalog_product_entity_varchar'] . ".entity_id
                LEFT JOIN " . $this->_tables['b1_linked_items'] . " ON " . $this->_tables['catalog_product_entity'] . ".entity_id = " . $this->_tables['b1_linked_items'] . ".shop_product_id
                WHERE attribute_id = 73 AND `shop_product_id` IS NULL 
                ORDER BY `value` ASC LIMIT " . $from . ", " . $items;
        $result = $this->_connection->fetchAll($sql);
        return $result;
    }

    public function getUnlinkedItemsCount()
    {
        $sql = "SELECT COUNT(*) as count FROM `" . $this->_tables['catalog_product_entity'] . "` 
                 LEFT JOIN " . $this->_tables['b1_linked_items'] . " ON " . $this->_tables['catalog_product_entity'] . ".entity_id = " . $this->_tables['b1_linked_items'] . ".shop_product_id
                 WHERE `shop_product_id` IS NULL";
        $result = $this->_connection->fetchAll($sql)[0]['count'];
        return $result;
    }

    public function getB1Items($from, $items, $relation)
    {
        if ($relation == 'more_1') {
            $sql = "SELECT * FROM `" . $this->_tables['b1_items'] . "` LEFT JOIN `" . $this->_tables['b1_linked_items'] . "` ON id = b1_product_id ORDER BY `name` ASC LIMIT " . htmlentities($from) . ", " . htmlentities($items);
        } else {
            $sql = "SELECT * FROM `" . $this->_tables['b1_items'] . "` LEFT JOIN `" . $this->_tables['b1_linked_items'] . "` ON id = b1_product_id WHERE `shop_product_id` IS NULL ORDER BY `name` ASC LIMIT " . htmlentities($from) . ", " . htmlentities($items);
        }
        $result = $this->_connection->fetchAll($sql);
        return $result;
    }

    public function getB1ItemsCountTable($relation)
    {
        if ($relation == 'more_1') {
            $sql = "SELECT COUNT(*) as count FROM `" . $this->_tables['b1_items'] . "` LEFT JOIN `" . $this->_tables['b1_linked_items'] . "` ON id = b1_product_id";
        } else {
            $sql = "SELECT COUNT(*) as count FROM `" . $this->_tables['b1_items'] . "` LEFT JOIN `" . $this->_tables['b1_linked_items'] . "` ON id = b1_product_id WHERE `shop_product_id` IS NULL";
        }
        $result = $this->_connection->fetchAll($sql);
        return $result[0]['count'];
    }

    public function getLinkedItems($from, $items)
    {
        $sql = "SELECT * FROM `" . $this->_tables['b1_linked_items'] . "` ORDER BY `shop_product_name` ASC LIMIT " . htmlentities($from) . ", " . htmlentities($items);
        return $this->_connection->fetchAll($sql);
    }

    public function getLinkedItemsCount()
    {
        $sql = "SELECT COUNT(*) as count FROM `" . $this->_tables['b1_linked_items'] . "` ";
        $result = $this->_connection->fetchAll($sql);
        return $result[0]['count'];
    }

    public function addItem($name, $code, $id)
    {
        $sql = "INSERT IGNORE INTO `" . $this->_tables['b1_items'] . "` (`name`, `code`, `id`) VALUES ('" . $name . "', '" . $code . "', '" . $id . "')";
        $this->_connection->query($sql);
    }

    public function linkProduct($shop_id, $b1_id)
    {
        if ((htmlspecialchars($shop_id) != 0) && (htmlspecialchars($b1_id) != 0)) {
            $product = $this->_connection->fetchAll("SELECT * FROM `" . $this->_tables['catalog_product_entity_varchar'] . "` WHERE `attribute_id` = 73 AND `entity_id` = '" . htmlspecialchars($shop_id) . "'")[0]['value'];
            $b1_product = $this->_connection->fetchAll("SELECT * FROM `" . $this->_tables['b1_items'] . "` WHERE `id` = '" . htmlspecialchars($b1_id) . "'");
            $sql = "INSERT INTO `" . $this->_tables['b1_linked_items'] . "`
                   (`shop_product_id`, `b1_product_id`, `shop_product_name`, `b1_product_name`, `b1_product_code`) VALUES 
                   ('" . (int)htmlspecialchars($shop_id) . "', '" . (int)htmlspecialchars($b1_id) . "', '" . $product . "', '" . $b1_product[0]['name'] . "', '" . $b1_product[0]['code'] . "')";
            $this->_connection->query($sql);
        }
    }

    public function unlinkProduct($product_id)
    {
        $this->_connection->query("DELETE FROM `" . $this->_tables['b1_linked_items'] . "` WHERE `shop_product_id` = " . htmlspecialchars($product_id));
    }

    public function resetItems()
    {
        $this->_connection->query("TRUNCATE TABLE `" . $this->_tables['b1_linked_items'] . "`");
    }

    public function getShopItemsCount()
    {
        $sql = "SELECT COUNT(*) as count FROM " . $this->_tables['catalog_product_entity'];
        $result = $this->_connection->fetchAll($sql);
        return $result[0]['count'];
    }

    public function getB1ItemsCount()
    {
        $sql = "SELECT COUNT(*) as count FROM " . $this->_tables['b1_items'];
        $result = $this->_connection->fetchAll($sql);
        return $result[0]['count'];
    }

    public function getB1Item($id)
    {
        return $this->_connection->fetchAll("SELECT * FROM `" . $this->_tables['b1_linked_items'] . "` WHERE `b1_product_id` = ' " . htmlspecialchars($id) . "'");
    }

    public function getB1ItemByShopId($id)
    {
        return $this->_connection->fetchAll('SELECT * FROM ' . $this->_tables['b1_linked_items'] . ' WHERE shop_product_id =' . htmlspecialchars($id));
    }

    public function quantityUpdate($quantity, $item_id)
    {
        $this->_connection->query("UPDATE `" . $this->_tables['cataloginventory_stock_item'] . "` SET `qty` = '" . htmlspecialchars($quantity) . "' WHERE `item_id` = '" . htmlspecialchars($item_id) . "'");
    }

    public function deleteLinkedItems($b1_ids)
    {
        $this->_connection->query("DELETE FROM  " . $this->_tables['b1_linked_items'] . " WHERE b1_product_id NOT IN (" . implode(',', $b1_ids) . ")");
    }

    public function deleteB1Items($b1_ids)
    {
        $this->_connection->query("DELETE FROM  " . $this->_tables['b1_items'] . " WHERE id NOT IN (" . implode(',', $b1_ids) . ")");
    }

}
