<?php

namespace Profis\B1\Model;

use Items\Framework\App\Config\Value;

class Clients extends \Magento\Framework\App\Config\Value
{

    /**
     * @var string
     */
    public $configPath = "advanced/modules_disable_output/Profis_B1";

    /**
     * @var \Magento\Framework\Model\Context
     */
    private $_context;

    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    private $_resourceConfig;
    private $_connection;
    private $_tables;
    protected $_config;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\MutableScopeConfigInterface $_config,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {

        $this->_context = $context;
        $this->_config = $_config;
        $this->_resourceConfig = $resourceConfig;
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $this->_connection = $this->_resources->getConnection();
        $this->_tables = array();
        $this->_tables['b1_clients'] = $this->_resources->getTableName('b1_clients');

        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * @return \Magento\Framework\App\Config\Value
     */
    public function save()
    {
        $this->_disabledModule(!$this->getValue());

        return parent::save();
    }

    /**
     * @param $disable
     */
    private function _disabledModule($disable)
    {
        $this->_resourceConfig->saveConfig($this->configPath, $disable, $this->getScope(), $this->getScopeId());
    }

    public function addClient($b1_client_id, $shop_client_id)
    {
        $sql = "INSERT IGNORE INTO `" . $this->_tables['b1_clients'] . "` (`b1_client_id`, `shop_client_id`) VALUES ('" . intval($b1_client_id) . "', '" . intval($shop_client_id) . "')";
        $this->_connection->query($sql);
    }

    public function getB1Client($shop_client_id)
    {
        return $result = $this->_connection->fetchAll("SELECT * FROM `" . $this->_tables['b1_clients'] . "` WHERE `shop_client_id` = '" . intval($shop_client_id) . "'");
    }
}
