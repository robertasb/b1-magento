<?php

namespace Profis\B1\Model;

use Items\Framework\App\Config\Value;

class Helper extends \Magento\Framework\App\Config\Value
{

    /**
     * @var string
     */
    public $configPath = "advanced/modules_disable_output/Profis_B1";

    /**
     * @var \Magento\Framework\Model\Context
     */
    private $_context;

    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    private $_resourceConfig;
    private $_connection;
    protected $_config;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\App\Config\ScopeConfigInterface $config, \Magento\Config\Model\ResourceModel\Config $resourceConfig, \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList, \Magento\Framework\App\Config\MutableScopeConfigInterface $_config, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = []
    )
    {

        $this->_context = $context;
        $this->_config = $_config;
        $this->_resourceConfig = $resourceConfig;
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $this->_connection = $this->_resources->getConnection();

        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    public function printPre($value)
    {
        sprintf("<pre>%s</pre>", print_r($value, true));
    }

}
