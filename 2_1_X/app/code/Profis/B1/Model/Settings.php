<?php

namespace Profis\B1\Model;

use Magento\Framework\App\Config\Value;

class Settings extends \Magento\Framework\App\Config\Value
{

    /**
     * @var string
     */
    public $configPath = "advanced/modules_disable_output/Profis_B1";

    /**
     * @var \Magento\Framework\Model\Context
     */
    private $_context;

    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    private $_resourceConfig;
    protected $_config;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\MutableScopeConfigInterface $_config,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {

        $this->_context = $context;
        $this->_config = $_config;
        $this->_resourceConfig = $resourceConfig;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * @return \Magento\Framework\App\Config\Value
     */
    public function save()
    {
        $this->_disabledModule(!$this->getValue());

        return parent::save();
    }

    /**
     * @param $disable
     */
    private function _disabledModule($disable)
    {
        $this->_resourceConfig->saveConfig($this->configPath, $disable, $this->getScope(), $this->getScopeId());
    }

    public function getValue($path)
    {
        return $this->_config->getValue($path);
    }

    public function setValue($path, $value)
    {
        $this->_resourceConfig->saveConfig($path, $value, 'default', 0);
    }

}
