<?php

namespace Profis\B1\Model;

use Items\Framework\App\Config\Value;

class Orders extends \Magento\Framework\App\Config\Value
{

    /**
     * @var string
     */
    public $configPath = "advanced/modules_disable_output/Profis_B1";

    /**
     * @var \Magento\Framework\Model\Context
     */
    private $_context;

    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    private $_resourceConfig;
    private $_connection;
    private $_tables;
    protected $_config;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\MutableScopeConfigInterface $_config,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {

        $this->_context = $context;
        $this->_config = $_config;
        $this->_resourceConfig = $resourceConfig;
        $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $this->_connection = $this->_resources->getConnection();
        $this->_tables = array();
        $this->_tables['catalog_product_entity'] = $this->_resources->getTableName('catalog_product_entity');
        $this->_tables['catalog_product_entity_varchar'] = $this->_resources->getTableName('catalog_product_entity_varchar');
        $this->_tables['b1_items'] = $this->_resources->getTableName('b1_items');
        $this->_tables['b1_orders'] = $this->_resources->getTableName('b1_orders');
        $this->_tables['b1_linked_items'] = $this->_resources->getTableName('b1_linked_items');
        $this->_tables['cataloginventory_stock_item'] = $this->_resources->getTableName('cataloginventory_stock_item');
        $this->_tables['sales_order'] = $this->_resources->getTableName('sales_order');
        $this->_tables['core_config_data'] = $this->_resources->getTableName('core_config_data');

        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * @return \Magento\Framework\App\Config\Value
     */
    public function save()
    {
        $this->_disabledModule(!$this->getValue());

        return parent::save();
    }

    /**
     * @param $disable
     */
    private function _disabledModule($disable)
    {
        $this->_resourceConfig->saveConfig($this->configPath, $disable, $this->getScope(), $this->getScopeId());
    }

    public function resetOrdersSync()
    {
        $this->_connection->query("UPDATE `" . $this->_tables['b1_orders'] . "` SET `b1_sync_id` = NULL WHERE b1_sync_id = 0");
    }

    public function getFailedOrders()
    {
        $sql = "SELECT COUNT(*) as count FROM " . $this->_tables['b1_orders'] . " WHERE b1_sync_id IS NOT NULL";
        return $this->_connection->fetchAll($sql)[0]['count'];
    }

    public function resetNotSyncB1Orders()
    {
        $this->_connection->query("UPDATE `" . $this->_tables['b1_orders'] . "` SET `next_sync` = NULL, `b1_sync_count` = 0  WHERE next_sync < '" . date('Y-m-d H:i:s') . "'");
    }

    public function hideOrdersFromSyncByDate($date)
    {
        $this->_connection->query("UPDATE " . $this->_tables['b1_orders'] . " LEFT JOIN " . $this->_tables['sales_order'] . " ON entity_id = shop_order_id SET `b1_order_id` = 0 WHERE `status` = 'complete' AND `b1_order_id` IS NULL AND `updated_at` < '" . htmlspecialchars($date) . "'");
    }

    public function setSyncId($id, $ttl, $thresold)
    {
        $this->_connection->query("UPDATE " . $this->_tables['b1_orders'] . " SET `b1_sync_id` = " . $id . " WHERE ((b1_order_id IS NULL AND b1_sync_id IS NULL) OR ($id - b1_sync_id > " . $ttl . " AND b1_sync_count < " . $thresold . "))  AND (next_sync < '" . date('Y-m-d H:i:s') . "' OR next_sync IS NULL)");
    }

    public function getSyncOrders($orders_sync_from, $id, $iteration)
    {
        return $this->_connection->fetchAll("SELECT * FROM " . $this->_tables['sales_order'] . " LEFT JOIN " . $this->_tables['b1_orders'] . " ON entity_id = shop_order_id  WHERE (`status` = 'complete' AND `updated_at` >= '" . htmlspecialchars($orders_sync_from) . "') AND ((b1_sync_id = " . $id . ") OR (b1_order_id IS NULL)) AND next_sync IS NULL ORDER BY updated_at LIMIT " . $iteration);
    }

    public function addB1Order($shop_order_id, $b1_order_id)
    {
        $this->_connection->query("INSERT IGNORE INTO `" . $this->_tables['b1_orders'] . "` (`b1_order_id`, `shop_order_id`, `b1_sync_count` , `b1_sync_id` ) VALUES (NULL, '" . $shop_order_id . "', 0,  '" . $b1_order_id . "')");
    }

    public function setOrderReference($b1_order_id, $shop_order_id)
    {
        $this->_connection->query("UPDATE `" . $this->_tables['b1_orders'] . "` SET `b1_sync_id` = NULL , `b1_order_id` = '" . htmlspecialchars((int)$b1_order_id) . "' WHERE `shop_order_id` = '" . htmlspecialchars((int)$shop_order_id) . "'");
    }

    public function setFailedSync($order)
    {
        $this->_connection->query("UPDATE `" . $this->_tables['b1_orders'] . "` SET `b1_sync_id` = NULL , `b1_sync_count` = b1_sync_count + 1, `next_sync` = IF(b1_sync_count >= '10', '" . date('Y-m-d H:i:s', strtotime('6 hour')) . "' , NULL)  WHERE `shop_order_id` = '" . htmlspecialchars((int)$order) . "'");
    }

    public function unsetSyncOrders($id)
    {
        $this->_connection->query("UPDATE " . $this->_tables['b1_orders'] . " SET `b1_sync_id` = NULL WHERE `b1_sync_id` = '$id'");
    }

    public function resetOrders()
    {
        $this->_connection->query("TRUNCATE TABLE `" . $this->_tables['b1_orders'] . "`");
        $this->_connection->query("UPDATE `" . $this->_tables['core_config_data'] . "` SET `value` = '0' WHERE `path` = 'b1/initial_sync'");
    }

}
