<?php

namespace Profis\B1\Controller\Adminhtml\Dashboard;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{

    /**
     * ACL resource
     */
    const ADMIN_RESOURCE = 'Profis_B1::dashboard';

    /**
     * @var PageFactory
     */
    private $resultPageFactory;
    private $_modelSettings;
    private $_modelItems;
    private $_cacheTypeList;
    private $_cacheFrontendPool;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context, PageFactory $resultPageFactory, \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList, \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool, \Profis\B1\Model\Settings $modelSettings, \Profis\B1\Model\Items $modelItems
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_modelSettings = $modelSettings;
        $this->_modelItems = $modelItems;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /**
         * @var \Magento\Backend\Model\View\Result\Page $resultPage
         */
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($this->getRequest()->getParam('draw')) {
                $pagination_data = array();
                $pagination_data['draw'] = $this->getRequest()->getParam('draw');
                $pagination_data['type'] = $this->getRequest()->getParam('type');
                $pagination_data['start'] = $this->getRequest()->getParam('start');
                $pagination_data['length'] = $this->getRequest()->getParam('length');

                if ($pagination_data['type'] == 'pagination_shop') {
                    $ajax_sorting_data = $this->_modelItems->getUnlinkedItems($pagination_data['start'], $pagination_data['length']);
                    $total = $this->_modelItems->getUnlinkedItemsCount();
                    foreach ($ajax_sorting_data as $item) {
                        $pagination_data['data'][] = array('name' => $item['value'], 'id' => $item['entity_id'], 'code' => $item['sku']);
                    }
                }
                if ($pagination_data['type'] == 'pagination_b1') {
                    $products_linked_db_values = array();
                    $products_linked_db_values[] = 0;
                    $ajax_sorting_data = $this->_modelItems->getB1Items($pagination_data['start'], $pagination_data['length'], $this->_modelSettings->getValue('b1/relation_type'));
                    $total = $this->_modelItems->getB1ItemsCountTable($this->_modelSettings->getValue('b1/relation_type'));
                    foreach ($ajax_sorting_data as $item) {
                        $pagination_data['data'][] = array('name' => $item['name'], 'id' => $item['id'], 'code' => $item['code']);
                    }
                }

                if ($pagination_data['type'] == 'pagination_linked') {
                    $ajax_sorting_data = $this->_modelItems->getLinkedItems($pagination_data['start'], $pagination_data['length']);
                    $total = $this->_modelItems->getLinkedItemsCount();
                    foreach ($ajax_sorting_data as $item) {
                        $pagination_data['data'][] = array('name' => $item['shop_product_name'], 'b1_name' => $item['b1_product_name'], 'id' => $item['shop_product_id'], 'b1_reference_id' => $item['b1_product_id'], 'upc' => $item['b1_product_code']);
                    }
                }

                $pagination_data['recordsTotal'] = $total;
                $pagination_data['recordsFiltered'] = $total;

                if ($pagination_data['recordsTotal'] == 0) {
                    $pagination_data['data'] = array();
                }
                $pagination_data = json_encode($pagination_data);
                printf("%s", $pagination_data);
            }
            if ($this->getRequest()->getParam('form') == 'link_product') {
                if (is_array($this->getRequest()->getParam('shop_item'))) {
                    foreach ($this->getRequest()->getParam('shop_item') as $item) {
                        $this->_modelItems->linkProduct(htmlspecialchars($item), htmlspecialchars($this->getRequest()->getParam('b1_item')));
                    }
                } else {
                    $this->_modelItems->linkProduct(htmlspecialchars($this->getRequest()->getParam('shop_item')), htmlspecialchars($this->getRequest()->getParam('b1_item')));
                }
            }
            if ($this->getRequest()->getParam('form') == 'unlink_product') {
                $this->_modelItems->unlinkProduct(htmlspecialchars($this->getRequest()->getParam('shop_item')));
            }
            if ($this->getRequest()->getParam('form') == 'reset_all') {
                $this->_modelItems->resetItems();
            }
            if ($this->getRequest()->getParam('form') == 'reset_all_orders') {
                $this->_modelOrders->resetOrders();
            }
            return false;
        }
        if ($this->getRequest()->getParam('shop_identifier')) {
            $shop_identifier = $this->getRequest()->getParam('shop_identifier');
            $orders_sync_from = $this->getRequest()->getParam('orders_sync_from');
            $cron_key = $this->getRequest()->getParam('cron_key');
            $api_key = $this->getRequest()->getParam('api_key');
            $relation_type = $this->getRequest()->getParam('relation_type');
            $private_key = $this->getRequest()->getParam('private_key');
            $tax_rate = $this->getRequest()->getParam('tax_rate');
            $confirmed_order_status = $this->getRequest()->getParam('confirmed_order_status');

            $this->_modelSettings->setValue('b1/relation_type', $relation_type);
            $this->_modelSettings->setValue('b1/shop_identifier', $shop_identifier);
            $this->_modelSettings->setValue('b1/orders_sync_from', $orders_sync_from);
            $this->_modelSettings->setValue('b1/cron_key', $cron_key);
            $this->_modelSettings->setValue('b1/api_key', $api_key);
            $this->_modelSettings->setValue('b1/private_key', $private_key);
            $this->_modelSettings->setValue('b1/confirmed_order_status', $confirmed_order_status);
            $this->_modelSettings->setValue('b1/tax_rate', $tax_rate);
            header('Location:' . 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

            $types = array('config', 'layout', 'block_html', 'collections', 'reflection', 'db_ddl', 'eav', 'config_integration', 'config_integration_api', 'full_page', 'translate', 'config_webservice');
            foreach ($types as $type) {
                $this->_cacheTypeList->cleanType($type);
            }
            foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                $cacheFrontend->getBackend()->clean();
            }
            return false;
        }
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Profis_B1::dashboard')
            ->addBreadcrumb(__('Sales'), __('Sales'))
            ->addBreadcrumb(__('B1 accounting'), __('B1 accounting'));
        $resultPage->getConfig()->getTitle()->prepend(__('B1 Accounting'));
        return $resultPage;
    }

    /**
     * @return bool
     */
    // @codingStandardsIgnoreLine
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE);
    }

}
