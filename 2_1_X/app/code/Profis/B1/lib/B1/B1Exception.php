<?php

/**
 * Base exception class for all exceptions in this library
 */

namespace Profis\B1\lib\B1;

class B1Exception extends \Exception
{

    /**
     * @var array
     */
    private $extraData;

    public function __construct($message = "", $extraData = [], $code = 0, \Exception $previous = null)
    {
        $this->extraData = $extraData;
        parent::__construct($message, $code, $previous);
    }

    public function getExtraData()
    {
        return $this->extraData;
    }

}
