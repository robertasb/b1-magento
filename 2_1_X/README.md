Įskiepis skirtas sinchronizuoti produktus tarp OpenCart ir B1.lt aplikacijos.

### Reikalavimai ###

* PHP 7.0.4
* Magento 2.1.x
* MySQL 5.7.15

### Diegimas ###

* NEBŪTINA Pasidarykite failų atsarginę kopiją.
* Padarykite atsarginę DB kopiją.
* Perkelkite `app`, `lib` direktorijas į pagrindinę magento direktoriją.
* Konsolės pagalba įvykdykite šias komandas 
    'php bin/magento setup:upgrade'
    'php bin/magento setup:di:compile'
* Administracijos skiltyje patikrinkite ar įjungtas Profis_B1 modulis (System->Configuration->Advance), jeigu neįjungttas - įjunkite.
* Administracijos skiltyje (Sales->B1) suveskite visą reikalingą informaciją.
* Administracijos skiltyje 'Orders sync from' laukelyje nurodykite data, nuo kurios bus sinchromnizuojami užsakymai. Datos formatas Y-m-d. Pvz. 2016-12-10 
* Administracijos skiltyje 'VAT Tax rate' laukelyje, nurodykite taikomą PVM mokestį (0%, 9%, 21%).
* Administracijos skiltyje 'Items relations for link' nurodykite prekių susiejimo būdą:
    * One to one - surišama vieną parduotuvės prekė su viena B1 preke, kiekiai yra sinchronizuojami
    * More to one - surišamos keletas parduotuvės prekių su viena B1 preke, kiekiai nėra sinchronizuojami
* Sutvarkykite cron vykdymą, jeigu nėra dar nustatyta. (http://devdocs.magento.com/guides/v2.0/config-guide/cli/config-cli-subcommands-cron.html)
* Įvykdykite cron užklausas.
* Susiekite B1 ir e.parduotuvės prekės "Unlinked Items" skiltyje.

### Kontaktai ###

* Kilus klausimams, prašome kreiptis info@b1.lt