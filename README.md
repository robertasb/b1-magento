Įskiepis skirtas sinchronizuoti produktus tarp Magento ir B1.lt aplikacijos.

### Reikalavimai 2.1.x ###

* PHP 7.0.4
* Magento 2.1.x
* MySQL 5.7.15

### Reikalavimai  1.9 ###

* PHP 5.5
* Magento 1.9.2.0

### Diegimas 2.1.X ###

* NEBŪTINA Pasidarykite failų atsarginę kopiją.
* Padarykite atsarginę DB kopiją.
* Perkelkite `2_1_X/app`, `2_1_X/lib` direktorijas į pagrindinę magento direktoriją.
* Konsolės pagalba įvykdykite šias komandas 
    'php bin/magento setup:upgrade'
    'php bin/magento setup:di:compile'
* Administracijos skiltyje patikrinkite ar įjungtas Profis_B1 modulis (System->Configuration->Advance), jeigu neįjungttas - įjunkite.
* Administracijos skiltyje (Sales->B1) suveskite visą reikalingą informaciją.
* Administracijos skiltyje 'Orders sync from' laukelyje nurodykite data, nuo kurios bus sinchromnizuojami užsakymai. Datos formatas Y-m-d. Pvz. 2016-12-10 
* Administracijos skiltyje 'VAT tax rate ID' laukelyje nurodykite jeigu taikomas PVM mokėstis, mokėsčio ID.
* Administracijos skiltyje 'Items relations for link' nurodykite prekių susiejimo būdą:
    * One to one - surišama vieną parduotuvės prekė su viena B1 preke, kiekiai yra sinchronizuojami
    * More to one - surišamos keletas parduotuvės prekių su viena B1 preke, kiekiai nėra sinchronizuojami
* Sutvarkykite cron vykdymą, jeigu nėra dar nustatyta. (http://devdocs.magento.com/guides/v2.0/config-guide/cli/config-cli-subcommands-cron.html)
* Įvykdykite cron užklausas.
* Susiekite B1 ir e.parduotuvės prekės "Unlinked Items" skiltyje.

### Diegimas 1.9 ###

* SVARBU! Perskaitykite [dokumentacija](https://www.b1.lt/help/e-shop-module-settings) ir atlikite nurodytus ten veiksmus.
* [NEBŪTINA] Pasidarykite failų atsarginę kopiją.
* Padaryti atsarginę DB kopiją.
* Išpakuokite archyvą. Atidarykite "1_9/app/code/local/B1/Accounting/etc/config.xml". Raskite <order_sync_from>2015-01-01 00:00:00</order_sync_from> ir pakeiskite nuo kokios datos bus perduodami užsakymai į B1. Išsaugokite. Pvz: Tik užsakymai kurie buvo sukurti po "2015-01-01 00:00:00" bus sinchronizuojami su B1.
* Perkelkite failus iš "1_9" direktorijos į magento direktoriją.
* Administracijos skiltyje atidarykite "System/Configuration/B1 plugin/Configuration settings" ir suveskite reikiamą informaciją. Pastaba: Jeigu nerandate "B1 plugin" punkto - pabandykite išvalyti magento cache'a (System/Cache management/Clear magento cache) ir prisijungti iš naujo.
* Nustatykite cron (System/Configuration/B1 plugin/Configuration settings/CRON rules) vykdymo laikus. Pastaba: Magento cron vykdytojas (System/Configuration/Advanced/System/Cron (Scheduled Tasks) - all the times are in minutes) gali ne iš karto sugeneruoti darbus.
* Cron darbai (Sync Orders, Fetch all products) turi susinchronizuoti užsakymus ir gauti prekės iš B1.
* Paspauskite "B1 Accounting/Unlinked products" ir susiekite produktus.

### Pastabos  1.9 ###

* Log failai:
    * /var/log/b1.log
    * /var/log/exceptions.log
* Į log failus plugin'as rašo tik jeigu jam tai leista daryti (System/Configuration/Advanced/Developer/Log settings; Enabled=true)

### Kontaktai ###

* Kilus klausimams, prašome kreiptis info@b1.lt